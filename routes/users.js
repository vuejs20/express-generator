const express = require('express')
const User = require('../models/User')
const router = express.Router()
// s
// let lastID = 11
const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource')
})
const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'user not found'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addUser = async function (req, res, next) {
  console.log(req.body)
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const updateUser = async function (req, res, next) {
  const userID = req.params.id
  try {
    const user = await User.findById(userID)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}
const deleteUser = async function (req, res, next) {
  const userid = req.params.id
  try {
    await User.findByIdAndDelete(userid)
    res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: 'user not found'
    })
  }
}
router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', addUser)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
